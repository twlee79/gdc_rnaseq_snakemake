configfile: "prepare_input/prepare_input_default_config.yaml" # defaults for prepare_input.smk
configfile: "default_config.yaml"  # default configuration parameters
configfile: "override_config.yaml" # overrides any default configuration parameters

include: "prepare_input/prepare_input.smk" # use prepare_input generic workflow to read sample_list and get list of input files

# pretty printer
pi_pp = pprint.PrettyPrinter(indent=4)

# genome list
genome_name = config['genome_name']
genome__list = [genome_name]

# add suffixes and presets to templater objects
for templater in (path_templater,input_source_templater):
    templater.add_alt_suffixes(config['path_templater_additional_extensions'])
    templater.add_preset_formats({'fastqc_file': {'logdir': ([],{}), 'new_affix' : (["_fastqc"],{}), 'new_suffix':([".html"],{}) },
                                  'all_inputs': {'apply_format': ([],{'inputid':"all"}) }
                                 })

# simple templaters for templating without sample_names etc.
basic_templater = path_templater.create("{directory}/{filename}")
    # simple directory/filename templater
    # defaults to intermediate dir

basic_fastq_gz_templater = basic_templater.fastq_gzfile()
basic_fastqc_templater = basic_templater.fastqc_file()
basic_bam_templater = basic_templater.bamfile()

# templater strings with end/genome appended
sample_inputid_genome_template = sample_inputid_template+"_{genome}"
sample_inputid_genome_expand_template = sample_inputid_expand_template+"_{genome}"

end_label_affix = "_" + end_label_wildcard
sample_inputid_endx_genome_template = sample_inputid_genome_template + end_label_affix

sample_inputid_endx_expand_template = sample_inputid_expand_template + end_label_affix
sample_inputid_endx_genome_expand_template = sample_inputid_genome_expand_template + end_label_affix

wildcard_constraints: # constraint genome wildcard
    genome = "({})".format("|".join(genome__list)),
    sample_name = "([^_]*)" 
        # prevents __ being applied to sample name; needed if filenames have multiple underscores

# parse inputid into illumina naming elements
# used for adding details to bamfiles
# Illumina naming convention {SampleName}_{SXX SampleNumber}_{LXXX LaneNumber}_{RX Read}_001
parse_illumina_name = lambda sample_name__inputid: re.match("(?P<run>.*)__(?P<illumina_sample_name>.+)_(?P<illumina_sample_number>S\d+)_(?P<illumina_lane_number>L\d+)",sample_name__inputid).groupdict()

# genome templaters
# top-level directory templaters
genome__sourcestaged_templater = path_templater.inputdir().create_fromparts(config['genome__sourcestaged_base_directory'],"","")
genome__build_templater = path_templater.emptydir().create_fromparts(config['genome__build_base_directory'],"","")
genome__buildstaged_directory = config['genome__buildstaged_base_directory']
genome__buildstaged_templater = path_templater.inputdir().create_fromparts(genome__buildstaged_directory,"","")

# genome source templaters
genome__starindex_targz_templater = genome__sourcestaged_templater.new_template(config['genome__starindex_targz_wildcard_template'])
genome__sourceinfo_templater = genome__sourcestaged_templater.new_template(config['genome__sourceinfo_wildcard_template'])

genome_star__build_upperdir_templater = genome__build_templater.new_template(config['genome_star__upperdir_wildcard_template'])
genome_star__build_genomedir_templater = genome_star__build_upperdir_templater.new_affix(config['genome_star__genomedir_wildcard_template']).apply_affix()
genome_star__build_genomefile_out_templater = genome_star__build_genomedir_templater.new_affix("/Genome")
    # this is one file built in genome index; used as output in build steps to ensure they completed correctly


# genome reference stage templaters
# staging ensures these are placed in input/ and logs copied, rather than being left in original (external) build dir

# use pattern that redefines directories in above templaters
genome_star__buildstaged_upperdir_templater = genome_star__build_upperdir_templater.inputdir().new_directory(genome__buildstaged_directory)
genome_star__buildstaged_genomedir_templater = genome_star__build_genomedir_templater.inputdir().new_directory(genome__buildstaged_directory)

# templaters for all steps
inputs__stats_templater = input_source_templater.fastq_statsfile()
inputs__fastqc_templater = input_source_templater.fastqc_file()


allreads__trim_worker_out_templater = input_source_templater.new_directory(config['allreads__trim_output_directory']).new_suffix(".fq.gz")
allreads__trim_out_templater = allreads__trim_worker_out_templater.fastq_gzfile()
allreads__trim_fastqc_templater = allreads__trim_out_templater.logdir().fastqc_file()
allreads__trim_stats_templater = allreads__trim_worker_out_templater.fastq_statsfile()

end_number_wildcard = "{endnumber}" 
wildcard_constraints:
    endnumber="[12]"
    # this is a alternate wildcard for endnumber, it differs from endlabel
    # in that it simply contains a wildcard-restricted number
    # for use in steps that do not allow setting output to 
    # correctly-named {endlabel}

# star produces a set of files with fixed names derived from a basename
gdc__star_base_out_templater = path_templater.outputdir().create_fromparts(config['gdc__star_output_directory'],
    sample_inputid_genome_template, filename_affix = "_star", suffix = "").all_inputs()

gdc__star_prefix_out_templater = gdc__star_base_out_templater.new_affix("_star.")

gdc__star_aligned_out_templater = gdc__star_base_out_templater.new_suffix(".Aligned.out.bam")
gdc__star_counts_out_templater = gdc__star_base_out_templater.new_suffix(".ReadsPerGene.out.tab")
gdc__star_log_out_templater = gdc__star_base_out_templater.new_suffix(".Log.out")
gdc__star_logfinal_out_templater = gdc__star_base_out_templater.new_suffix(".Log.final.out")
gdc__star_logprogress_out_templater = gdc__star_base_out_templater.new_suffix(".Log.progress.out")
gdc__star_sj_out_templater = gdc__star_base_out_templater.new_suffix(".SJ.out.tab")
gdc__star_log_stdout_templater = gdc__star_base_out_templater.new_affix("_stdout").logfile()
gdc__star_aligned_transcriptome_out_templater = gdc__star_base_out_templater.new_suffix(".Aligned.toTranscriptome.out.bam")


analysis_complete_templater = path_templater.create_fromparts(config['logs__summary_output_directory'], config['logs__summary_base_filename'],"").outputdir().completefile()
summary_out_templater = path_templater.create_fromparts(config['logs__summary_output_directory'], config['logs__summary_base_filename'],"").outputdir().new_affix("_summary").new_suffix(".tsv")

# runs full workflow
rule all:
    input:
        analysis_complete_templater.use()

# executes full workflow
# output can be linked to a run summarisation step
rule run_all:
    input:
        gdc__star_counts_out_templater.expand(sample_name = sample_dict.keys(), genome=genome__list) # gdc star counts
    output:
        touch(analysis_complete_templater.use())

###########################
# List of generic resources
###########################
# mem_mb: memory usage, in mb
#   generally not used except if a rule in causing problems
resources_mem_mb_star_align = 20*1024 # default to 20 GiB
# disk_mb: disk (assume temporary) usage, in mb
#   generally not used except if a rule in causing problems
# disk_concurrent_io: concurrent disk i/o
#   arbitrary value of 1 for each file being read/written
# rule_concurrent: 1
#   all rules have value of 1 for this to allow limit of executing x rules 
#   at a time
# rule_intensive: 1, 2, 8 etc.
#   lacking for most rules
#   can be used as an arbitrary scale to limit number of concurrent
#   'intensive'

#######
# Rules
#######

# generic rule to generate stats on reads and bases in a fastqc.gz file
rule generic_fastq_stats:
    input:
        basic_fastq_gz_templater.use()
    output:
        basic_fastq_gz_templater.fastq_statsfile().use()
    resources:
        rule_concurrent = 1,
        disk_concurrent_io = 1
    shell:
        "scripts/summarise_fq.sh {input} >{output}"

# as above, but inputs in outputdir()
use rule generic_fastq_stats as generic_fastq_stats_output with:
    input:
        basic_fastq_gz_templater.outputdir().use()

# generic rule to run fastqc on a fastqc.gz file
rule generic_fastqc:
    input:
        basic_fastq_gz_templater.use()
    output:
        basic_fastqc_templater.use()
    log:
        basic_fastqc_templater.logfile().use()
    params:
        output_dir = basic_fastqc_templater.get_directory(),
        additional_parameters = config["generic_fastqc_additional_parameters"]
    conda:
        "envs/fastqc.yaml"
    resources:
        rule_concurrent = 1,
        disk_concurrent_io = 1
    shell:
        "fastqc -o {params.output_dir} {input} "
        " {params.additional_parameters} "
        " >{log} 2>&1"

# chain to this rule to get stats on inputs
rule fastq_stats_inputs:
    input:
        inputs__stats_templater.expand_ends()
    resources:
        rule_concurrent = 1
    output:
        touch(inputs__stats_templater.end1_2().completefile().use())

#abrije_add(inputs__stats_templater
#inputs__stats_templater, "type_tsv_firstrow"

# chain to perform fastqc on input reads
rule fastqc_inputs:
    input:
        inputs__fastqc_templater.expand_ends()
    resources:
        rule_concurrent = 1
    output:
        touch(inputs__fastqc_templater.end1_2().completefile().use())

# stages genome targz file by linking to within workflow dir
# copies source_info to within workflow dir
# generates md5 sum of linked files for data integrity check as resident log
# (resident log is located in input directory)
# duplicates sourceinfo into logs/genomesource
# duplicates resident log to logdir
rule genome__stage_source_files:
    input:
        starindex_targz_file = lambda wildcards: config['genome__starindex_targz_path_lookup'][wildcards.genome],
        sourceinfo_dir = lambda wildcards: config['genome__sourceinfo_path_lookup'][wildcards.genome]
    output:
        # this staging is distinct from staging references/indexes in that
        # we link individual files rather than directories
        # so output refers to these files
        # any consumers of these files should mark the input files with the
        # ancient() tag as snakemake will use assume these output files are
        # modified if this rule is called
        # this can be done by expanding the **genome__stage_source_files_outputs
        # dict below
        starindex_targz_file = genome__starindex_targz_templater.use(),
        sourceinfo_dir = directory(genome__sourceinfo_templater.use())
    log:
        resident = genome__sourceinfo_templater.residentlogfile().use(),
        withinlogdir_sourceinfo = directory(genome__sourceinfo_templater.logdir().use()),
        withinlogdir_log = genome__sourceinfo_templater.logfile().use()
    resources:
        rule_concurrent = 1,
        disk_concurrent_io = 1
    shell:
        "ln -Tsrv {input.starindex_targz_file} {output.starindex_targz_file} "
        " >{log.resident} 2>&1\n"
        "cp -pr {input.sourceinfo_dir} {output.sourceinfo_dir} "
        " >>{log.resident} 2>&1\n"
        "echo 'MD5 of files after staging:' >>{log.resident} 2>&1\n"
        "md5sum {output.starindex_targz_file} >>{log.resident} 2>&1\n"
        "cp -pr {output.sourceinfo_dir} {log.withinlogdir_sourceinfo}\n"
        "cp -pr {log.resident} {log.withinlogdir_log} "

genome__stage_source_files_outputs = {
    "starindex_targz_file" : ancient(rules.genome__stage_source_files.output.starindex_targz_file),
}

# returns a string consisting of all_inputids formatted for template joined by sep
# to use: lambda wildcards: expand_all_inputids_as_str(wildcards, template, sep)
def expand_all_inputids_as_str(wildcards, template, sep = " "):
    all_inputids = expand_all_inputids_function_factory(template)(wildcards)
    ret = sep.join(all_inputids)
    return ret


# build star index in genome__build_base_directory
# from source files in genome__sourcestaged_base_directory
rule star__extract_index:
    input:
        **genome__stage_source_files_outputs
    output:
        genome_star__build_genomefile_out_templater.use()
    log:
        source_log = genome_star__build_genomedir_templater.sourcelogfile().use(),
        build_log = genome_star__build_genomedir_templater.residentlogfile().use(),
    params:
        prefix = genome_star__build_genomedir_templater.use(),
        build_dir = genome_star__build_upperdir_templater.use(),
        genome__dir = genome_star__build_genomedir_templater.use(),
        standard_parameters = config["star__extract_index_untar_parameters"],
        analysis_parameters = config["star__extract_index_untar_additional_parameters"],
    threads:
        12
    resources:
        rule_concurrent = 1,
        disk_concurrent_io = 2,
        mem_mb = 40*1024 # building genome is quite memory-intensive
    shell:
        "echo 'MD5 of source files:' >{log.source_log} 2>&1\n"
        "md5sum {input.starindex_targz_file} >>{log.source_log} 2>&1\n"
        "tar -xvf {input.starindex_targz_file} -C {params.prefix} "
        " {params.standard_parameters} "
        " {params.analysis_parameters} "
        " >{log.build_log} 2>&1"

# stage star index in genome__buildstaged_base_directory
# from build in genome__build_base_directory
rule star__stage_index:
    input:
        **rules.star__extract_index.output,
        **rules.star__extract_index.log
    output:
        staging_complete = touch(genome_star__buildstaged_upperdir_templater.completefile().use())
    log:
        source_log = genome_star__buildstaged_genomedir_templater.sourcelogfile().logdir().use(),
        build_log = genome_star__buildstaged_genomedir_templater.residentlogfile().logdir().use(),
        stage_log = genome_star__buildstaged_genomedir_templater.new_affix("_stage").logfile().use(),
    params:
        **rules.star__extract_index.params,
        staged_build_dir = genome_star__buildstaged_upperdir_templater.use(),
        staged_genome_dir = genome_star__buildstaged_genomedir_templater.use(),
    resources:
        rule_concurrent = 1,
        disk_concurrent_io = 1
    shell:
        "cp {input.source_log} {log.source_log} >{log.stage_log} 2>&1\n"
        "cp {input.build_log} {log.build_log} >>{log.stage_log} 2>&1\n"
        "ln -Tsrv {params.build_dir} {params.staged_build_dir} >>{log.stage_log} 2>&1\n"

# define a dict for use as input for downstream rules using 
# original input files
# which includes these files plus associated stats and fastqc files. 
# includes ancient() tag on stats files to ensure updating stats files alone
# does not trigger rerunning workflow (might occur if stats were generated
# independently rather than directly from workflow).
# to use, unpack this dict with **; input files for end1 and 2 can be
# referenced with R1 and R2
allreads__input_files = {
    **{'R'+str(index+1):item for index,item in 
        enumerate(input_source_templater.expand_ends())},
    **{'stats'+str(index+1):ancient(item) for index,item in 
        enumerate(chain(
            rules.fastq_stats_inputs.input, 
            rules.fastqc_inputs.input
            ))}
}

# this rule is not normally included in workflow
# it is present to allow generation of stats files by calling for 
# its output to be generated
rule allreads__input_files:
    input:
        **allreads__input_files
    resources:
        rule_concurrent = 1
    output:
        touch(input_source_templater.end1_2().completefile().use())

# perform trim for quality and illumina adaptors
# trim_galore has this annoying fixed file naming scheme
rule allreads__trim_worker:
    input:
        **allreads__input_files
    output:
        R1 = allreads__trim_worker_out_templater.end1().new_affix("_val_1").use(),
        R2 = allreads__trim_worker_out_templater.end2().new_affix("_val_2").use()
    log:
        allreads__trim_worker_out_templater.end1_2().logfile().use()
    params:
        output_dir = allreads__trim_worker_out_templater.get_directory(),
        standard_parameters = config["allreads__trim_standard_parameters"],
        trim_parameters = config["allreads__trim_trim_parameters"],
        additional_parameters = config["allreads__trim_additional_parameters"]
    conda:
        "envs/trim_galore.yaml"
    resources:
        rule_concurrent = 1,
        disk_concurrent_io = 2
    shell:
        "trim_galore "
        " {params.standard_parameters} "
        " {params.trim_parameters} "
        " -o {params.output_dir} "
        " {params.additional_parameters} "
        " {input.R1} {input.R2}"
        " >{log} 2>&1"


# will run allreads__trim_worker, but convert non-standard file naming scheme of
# trim_galore output to standard
rule allreads__trim:
    input:
        **rules.allreads__trim_worker.output
    output:
        R1 = allreads__trim_out_templater.end1().use(),
        R2 = allreads__trim_out_templater.end2().use(),
    resources:
        rule_concurrent = 1
    shell:
        """
        mv {input.R1} {output.R1}
        mv {input.R2} {output.R2}
        """

# including inputs to this rule will trigger generating stats of
# allreads__trim
rule allreads__trim_stats:
    input:
        stats_R1 = allreads__trim_stats_templater.end1().use(),
        stats_R2 = allreads__trim_stats_templater.end2().use(),

# including inputs to this rule will trigger generating fastqc of
# allreads__trim
rule allreads__trim_fastqc:
    input:
        fastqc_R1 = allreads__trim_fastqc_templater.end1().use(),
        fastqc_R2 = allreads__trim_fastqc_templater.end2().use(),


# define a dict for use as input for downstream rules using 
# outputs of allreads__trim 
# which includes these files plus associated stats and fastqc files. 
# includes ancient() tag on stats files to ensure updating stats files alone
# does not trigger rerunning workflow (might occur if stats were generated
# independently rather than directly from workflow).
# to use, unpack this dict with **
allreads__trim_outputs = {
    **rules.allreads__trim.output,
    **{key:ancient(value) for (key, value) in 
        chain(rules.allreads__trim_stats.input.items(), 
              rules.allreads__trim_fastqc.input.items()
              )}
}    

# this rule is not normally included in workflow
# it is present to allow generation of stats files by calling for 
# its output to be generated
rule allreads__trim_outputs:
    input:
        **allreads__trim_outputs
    resources:
        rule_concurrent = 1
    output:
        touch(allreads__trim_out_templater.end1_2().completefile().use())


# select either allreads__trim_outputs or allreads__input_files outputs for 
# subsequent steps depending on if use_trim
gdc_input_fq_files = allreads__trim_outputs if config['use_trim'] else allreads__input_files

# generate read group line for all inputs given by sample_name in wildcards
# id includes lane number if config option 'gdc__star_add_lane_to_id'
# is set to true
def gdc__star_generate_rg(wildcards):
    inputids = get_all_inputids(wildcards.sample_name)
    LB = "{sample_name}.L1".format(**wildcards)  # library, one per sample
    SM = "{sample_name}".format(**wildcards)
    PL = "ILLUMINA"
    rg_per_inputids = []
    for inputid in inputids:
        ID = "{sample_name}.{illumina_lane_number}".format(**wildcards,**parse_illumina_name(inputid)) if (config['gdc__star_add_lane_to_id']) else "{sample_name}".format(**wildcards)
        rg_per_inputid = "ID:{id} LB:{LB} SM:{SM} PL:{PL}".format(id = ID, LB = LB, SM = SM, PL = PL)
        rg_per_inputids.append(rg_per_inputid)
    return " , ".join(rg_per_inputids)

# this function takes an dict of inputs (e.g for pattern using ancient() tag)
# for each item in the dict, it will generate a function to expand inputs
# using expand_all_inputids_function_factory(), but with the input to the
# function being a str version of the item value (i.e. removing flags), and
# then enclosing this function in an outer function to add flags;
# returning a list of expand functions
# i.e. {R1, item(flags: ancient)} -> 
#      [ancient(expand_all_inputids_function_factory(str(item)))]
# this is needed as snakemake requires flagging functions outside any expand
# functions
def expand_all_inputids_function_factor_fromdict(input_dict):
    ret = []
    for key,value in input_dict.items():
        expander = expand_all_inputids_function_factory(str(value))
        if hasattr(value, "flags") and value.flags:
            # if item has flags
            for flagname, flagvalue in value.flags.items():
                # typical format of flags: {'ancient': True}
                if flagvalue:
                    # assume we can find a flag-function in the globals
                    # e.g. ancient()
                    expander = globals()[flagname](expander)
        ret.append(expander)
    return ret


# perform star alignment of filtered reads (disambiguated/deduplicated)
# with a genome
rule gdc__star:
    input:
        expand_all_inputids_function_factor_fromdict(gdc_input_fq_files),
        **rules.star__stage_index.output
    output:
        bam = gdc__star_aligned_out_templater.use(),
        sj_table = gdc__star_sj_out_templater.use(),
        counts_table = gdc__star_counts_out_templater.use(),
        transcriptome_bam = gdc__star_aligned_transcriptome_out_templater.use()
    log:
        out = gdc__star_log_out_templater.use(),
        final_out = gdc__star_logfinal_out_templater.use(),
        progress_out = gdc__star_logprogress_out_templater.use(),
        stdout = gdc__star_log_stdout_templater.use()
    params:
        prefix = gdc__star_prefix_out_templater.use(),
        R1_list = lambda wildcards: expand_all_inputids_as_str(wildcards, gdc_input_fq_files['R1'], sep=","),
        R2_list = lambda wildcards: expand_all_inputids_as_str(wildcards, gdc_input_fq_files['R2'], sep=","),
        rg_line = lambda wildcards: gdc__star_generate_rg(wildcards),
        staged_genome_dir = rules.star__stage_index.params.staged_genome_dir,
        input_parameters = config["gdc__star_input_parameters"],
        output_parameters = config["gdc__star_output_parameters"],
        alignment_parameters = config["gdc__star_alignment_parameters"],
        transcriptome_parameters = config["gdc__star_transcriptome_parameters"],
        additional_parameters = config["gdc__star_additional_parameters"]
    conda:
        "envs/star.yaml"
    threads: 12
    resources:
        rule_concurrent = 1,
        disk_concurrent_io = 2,
        mem_mb = resources_mem_mb_star_align
    shell:
        "STAR "
        " --genomeDir {params.staged_genome_dir} "
        " --runThreadN {threads} "
        " --readFilesIn {params.R1_list} {params.R2_list} "
        " --readFilesCommand zcat "
        " {params.input_parameters} "
        " --outFileNamePrefix {params.prefix} "
        " {params.output_parameters} "
        " --outSAMattrRGline {params.rg_line} "
        " {params.alignment_parameters} "
        " {params.transcriptome_parameters} "
        " {params.additional_parameters} "
        " >{log.stdout} 2>&1"


# for debugging
if verbose>1: print_all_rules()
