Genomic Data Commons (GDC) pipeline using snakemake
---------------------------------------------------

Author: Tet-Woo Lee <tw.lee AT auckland.ac.nz>

Revision history:

* v0.5 2024-05-08
  - add optional trimming of input data 
  - rename repo to `gdc_rnaseq_snakemake`
* v0.42 2023-02-11
  - bugfix underscores in sample names
* v0.41 2023-02-11
  - default to adding lane id to RG 
  - forbid underscores in sample names
* v0.4 2023-02-11
  - first working version

Notes:

1. Use prebuilt GDC Star indexes.
2. Input files can be raw reads (suggest using trim) or disambiguated/deduplicated FASTQ files from previous `hnscc_clxpdx` RNASeq pipeline (trim not needed).
3. About 40 GB RAM is used for STAR align in this workflow.
4. Since filenames have underscores within them, underscores in sample names (which prepended to intermediate filenames) are not supported to prevent interfering with wildcard assignments.

Reference:
https://docs.gdc.cancer.gov/Data/Bioinformatics_Pipelines/Expression_mRNA_Pipeline/
`Dr32` pipeline is as follows:
```
# STAR Genome Index
STAR
--runMode genomeGenerate
--genomeDir <star_index_path>
--genomeFastaFiles <reference>
--sjdbOverhang 100
--sjdbGTFfile <gencode.v36.annotation.gtf>
--runThreadN 8

# STAR Alignment
# STAR v2
STAR
--readFilesIn <fastq_files> \
--outSAMattrRGline <read_group_strings> \
--genomeDir <genome_dir> \
--readFilesCommand <cat, zcat, etc> \
--runThreadN <threads> \
--twopassMode Basic \
--outFilterMultimapNmax 20 \
--alignSJoverhangMin 8 \
--alignSJDBoverhangMin 1 \
--outFilterMismatchNmax 999 \
--outFilterMismatchNoverLmax 0.1 \
--alignIntronMin 20 \
--alignIntronMax 1000000 \
--alignMatesGapMax 1000000 \
--outFilterType BySJout \
--outFilterScoreMinOverLread 0.33 \
--outFilterMatchNminOverLread 0.33 \
--limitSjdbInsertNsj 1200000 \
--outFileNamePrefix <output prefix> \
--outSAMstrandField intronMotif \
--outFilterIntronMotifs None \
--alignSoftClipAtReferenceEnds Yes \
--quantMode TranscriptomeSAM GeneCounts \
--outSAMtype BAM Unsorted \
--outSAMunmapped Within \
--genomeLoad NoSharedMemory \
--chimSegmentMin 15 \
--chimJunctionOverhangMin 15 \
--chimOutType Junctions SeparateSAMold WithinBAM SoftClip \
--chimOutJunctionFormat 1 \
--chimMainSegmentMultNmax 1 \
--outSAMattributes NH HI AS nM NM ch
```

